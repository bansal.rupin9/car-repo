import  { useState} from "react";
import LoginForm from "./components/LoginForm";
import FirstPage from "./components/FirstPage";

function App() {
  const adminUser = {
    email : "admin@ford.com",
    password : "byeindia"
  }

  const [user, setUser] = useState ({email : "", name : ""})
  const [err, setError] = useState ("")

  const Login = details => {
    console.log("details", details)
    if(details.email === adminUser.email && details.password === adminUser.password){
      console.log("admin")
      setUser({
        name : details.name,
        email: details.email 
      });
    }
    else {
      console.log("in else")
      setError("Details not matched.")
    }
  }

  const Logout = () => {
    setUser({ name : "", email : ""})
  }
  return (
    <div className="App">
      {(user.email !== "") ?
        (
        <div className = "welcome">
          <h2> Welcome , {user.name}</h2> 
          <button onClick = {Logout}>Logout </button>
          <FirstPage/>

        </div>
        )
        : (
          <div>
            <LoginForm Login = {Login} err = {err}/>
          </div>
        )
      }
    </div>
  );
}

export default App;
