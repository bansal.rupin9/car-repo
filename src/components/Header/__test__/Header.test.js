import { render, screen } from '@testing-library/react';
import Header from '../Header';

test('renders learn react link', () => {
  render(<Header  title =  "My App" />);
  const headingElement = screen.getByText(/My App/i);
  expect(headingElement).toBeInTheDocument();
});
