import { useState } from 'react'

import rawData from './../config/config'

function FirstPage() {
    const [searchCar, setSearchCar] = useState("")
    return (
        <section className="carList">
            <input
                className = "search"
                type="text"
                placeholder="Search..."
                onChange={(event) => {
                    setSearchCar(event.target.value)
                }} />
            {rawData.filter((val) => {
                if (searchCar === "") {
                    return val
                }
                else if (val.carName.toLowerCase().includes(searchCar.toLowerCase())) {
                    return val

                }
            }).map((carValue, index) => {
                const { image, carName, pattern } = carValue
                return (
                    <Cars key={carValue.id} carValue={carValue} />
                )
            })}
        </section>
    )


}

const Cars = (props) => {
    return (
        <article>
            <img src={props.carValue.image} alt='' />
            <h1>
                {props.carValue.carName.toUpperCase()}
            </h1>
            <h4> {props.carValue.pattern}</h4>
        </article>
    )
}

export default FirstPage
